#!/usr/bin/python3
import argparse
import json
import os
import sys
import subprocess

import ldap
import jinja2
import urllib

path = os.path.dirname(os.path.abspath(__file__))

def connect(config):
	base = ldap.initialize(config['uri'])
	if config['uri'].startswith('ldaps://'):
		base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
		base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
	if 'binddn' in config:
		base.simple_bind_s(config['binddn'],config['passwd'])
	return base


if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description="Generate user configuration for proxmox",
	)
	group = parser.add_mutually_exclusive_group()
	group.add_argument('-e', '--export', help='Export the user configuration to stdout', action='store_true')
	group.add_argument('-p', '--path', help='Select path to export user configuration', default='/etc/pve')
	args = parser.parse_args()

	with open(os.path.join(path, 'proxmox-user.json')) as file:
		config = json.load(file)

	base = {}
	users = { target: {} for target in config['ldap'] }
	for target,cfg in config['ldap'].items():
		base[target] = connect(cfg)
		field = 'uid' if cfg['realm'] == 'pam' else 'cn'
		users_qid = base[target].search(cfg['userBase'], ldap.SCOPE_ONELEVEL, 'objectClass=inetOrgPerson',
			attrlist=['givenName', 'sn', 'mail', 'userPassword', field])
		users_q = base[target].result(users_qid)[1]
		for dn, entries in users_q:
			uid = entries[field][0].decode('utf-8')
			users[target][uid] = {}
			for key in ['givenName', 'sn', 'mail', 'userPassword']:
				users[target][uid][key] = entries[key][0].decode('utf-8') if key in entries else ''
			for key in ['givenName', 'sn']:
				users[target][uid][key] = urllib.parse.quote(users[target][uid][key])

	groups = {}
	roles = config['roles'] if 'roles' in config else {}
	acls = []
	# Recover groups with admin privileges
	nounous_qid = base['admin'].search('dc=crans,dc=org', ldap.SCOPE_SUBTREE, 'cn=_nounou',
		attrlist=['memberUid'])
	nounous = base['admin'].result(nounous_qid)[1][0][1]
	nounous = { user.decode('utf-8') for user in nounous['memberUid'] }
	apprentis = set(users['admin'].keys()) - nounous
	groups['nounou'] = ','.join(f'{u}@pam' for u in nounous)
	groups['apprenti'] = ','.join(f'{u}@pam' for u in apprentis)
	acls.append({ 'propagate': 1, 'role': 'Administrator', 'target': '@nounou', 'path': '/'})
	acls.append({ 'propagate': 1, 'role': 'PVEAuditor', 'target': '@apprenti', 'path': '/'})

	# Recover clubs
	if 'user' in config['ldap']:
		clubs_qid = base['user'].search('ou=clubs,dc=adh,dc=crans,dc=org',
			ldap.SCOPE_ONELEVEL, 'objectClass=organization',
			attrlist=['description', 'o'])
		clubs_q = base['user'].result(clubs_qid)[1]
		for dn, entries in clubs_q:
			club = entries['o'][0].decode('utf-8')
			if 'description' in entries:
				groups[club] = ','.join(
					[ '{}@pve'.format(user.decode('utf-8')) for user in entries['description'] ]
				)
		hosts_qid = base['user'].search('ou=hosts,dc=adh,dc=crans,dc=org',
			ldap.SCOPE_ONELEVEL, '(&(objectClass=device)(serialNumber=*))',
			attrlist=['serialNumber', 'owner'])
		hosts_q = base['user'].result(hosts_qid)[1]
		for dn,host in hosts_q:
			vmid = host['serialNumber'][0].decode('utf-8')
			for owner in host['owner']:
				owner = owner.decode('utf-8').split(',')
				o = owner[0].split('=')[1]
				owner = f'@{o}' if owner[1] == 'ou=clubs' else '{}@{}'.format(o,config['ldap']['user']['realm'])
				acls.append({'propagate': 0, 'role': base['user']['pve_vm_role'], 'target': owner, 'path': f'/vms/{vmid}'})
				acls.append({'propagate': 1, 'role': 'PVEDatastoreUser', 'target': owner, 'path': f'/storage/local'})

	passwords = {}
	user_db = {}
	for target,cfg in config['ldap'].items():
		for user,info in users[target].items():
			user_db['{}@{}'.format(user,cfg['realm'])] = info
			if cfg['realm'] == 'pve':
				passwords[user] = info['userPassword'].replace('{CRYPT}','')
	users = user_db

	with open(os.path.join(path, 'templates', 'user.cfg.j2')) as template:
		user_template = jinja2.Template(template.read())
	with open(os.path.join(path, 'templates', 'priv', 'shadow.cfg.j2')) as template:
		shadow_template = jinja2.Template(template.read())
	if args.export:
		print('# +-------------+')
		print('# | -> user.cfg |')
		print('# +-------------+')
		print(user_template.render(users=users, groups=groups, acls=acls))
		print('')
		print('# +--------------------+')
		print('# | -> priv/shadow.cfg |')
		print('# +--------------------+')
		print(shadow_template.render(users=passwords))
	elif os.path.ismount('/etc/pve'):
		with open(os.path.join(args.path, 'user.cfg'), 'w') as file:
			file.write(user_template.render(users=users, roles=roles, groups=groups, acls=acls))
		with open(os.path.join(args.path, 'priv/shadow.cfg'), 'w') as file:
			file.write(shadow_template.render(users=passwords))
